var parent;
$(document).ready(function () {
    $('#zoom_btn_out').on('click',function () {
        $('#zoom').fadeOut();
        // $('#zoom').css('display','none');
        $('#zoom_img').remove();
    });

    $('.i').on('click',function () {
        parent = $(this).parent();
        // $('#photo_container').append($(this).clone().attr('id', 'zoom_img').attr('alt', '').attr('class', '').removeClass('i')); //.wrap("<div class='center_container'></div>")
        $('#photo_container').css('background-image','url("'+$(this).attr('src')+'")');
        // $('#zoom').css('display','block');
        $('#zoom').fadeIn();
    });

    $('#zoom_btn_right').on('click',function () {
        // alert('Right!');
        parent = parent.next();
        if (!parent.length){
            parent = $('ol li').first();
        }  
        // $('#zoom_img').replaceWith(parent.find('img').clone().attr('id', 'zoom_img').removeClass('i'));
        $('#photo_container').css('background-image','url("'+parent.find('img').attr('src')+'")');

    });

    $('#zoom_btn_left').on('click',function () {
        // alert('Left!');
        parent = parent.prev();
        if (!parent.length){
            parent = $('ol li').last();
        }
        $('#photo_container').css('background-image','url("'+parent.find('img').attr('src')+'")');
        // $('#zoom_img').replaceWith(parent.find('img').clone().attr('id', 'zoom_img').removeClass('i'));     
    });
});
    
